package note.test;

import static org.junit.Assert.*;

import java.util.List;

import note.model.Elev;
import note.model.Medie;
import note.model.Nota;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import note.utils.ClasaException;
import note.utils.Constants;

import note.controller.NoteController;

public class CalculeazaMediiTest {
	
private NoteController ctrl;
	
	@Before
	public void init(){
		ctrl = new NoteController();
	}
	
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	@Test
	public void test1() throws ClasaException{
		expectedEx.expect(ClasaException.class);
		expectedEx.expectMessage(Constants.emptyRepository);
		ctrl.calculeazaMedii();
	}
	
	@Test
	public void test3() throws ClasaException {
		Elev e1 = new Elev(1, "Elev1");
		ctrl.addElev(e1);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		List<Medie> rezultate = ctrl.calculeazaMedii();
		assertEquals(rezultate.size(), 1);
		Double d = rezultate.get(0).getMedie();
		assertTrue(d.isNaN());
	}
	
	@Test
	public void test4() throws ClasaException {
		Elev e1 = new Elev(1, "Elev1");
		Nota n = new Nota(1, "romana", Double.NaN);
		ctrl.addElev(e1);
		ctrl.addNota(n);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		List<Medie> rezultate = ctrl.calculeazaMedii();
		assertEquals(rezultate.size(), 1);
		Double d = rezultate.get(0).getMedie();
		assertTrue(d.isNaN());
	}

	@Test
	public void test5() throws ClasaException {
		Elev e1 = new Elev(1, "Elev1");
		ctrl.addElev(e1);
		Nota n1 = new Nota(1,"romana", 10);
		ctrl.addNota(n1);
		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		List<Medie> rezultate = ctrl.calculeazaMedii();
		for(Medie m : rezultate)
			if(m.getElev().getNrmatricol() == 1)
				assertEquals(m.getMedie(),10,0.0001);
	}
	
}
